import pytest as pytest
from selenium import webdriver
#from webdriver_manager.chrome import ChromeDriverManager

desiredCapabilities={
"browserName":"chrome"
}


@pytest.fixture(scope='function')
def driver():
    #driver = webdriver.Chrome(ChromeDriverManager().install())
    driver = webdriver.Remote(command_executor='http://192.168.31.97:4444/wd/hub',desired_capabilities = desiredCapabilities)
    driver.maximize_window()
    yield driver
    driver.quit()
